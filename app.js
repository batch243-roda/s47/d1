const firstName = document.querySelector("#firstName");
const lastName = document.querySelector("#lastName");
const fullName = document.querySelector("#fullName");
const colors = document.querySelector("#colors");

console.log(colors.children);
const showFullName = () => {
  fullName.textContent = `${firstName.value} ${lastName.value}`;
};

const changeColor = () => {
  fullName.style.color = `${colors.value}`;
};

firstName.addEventListener("keyup", showFullName);
lastName.addEventListener("keyup", showFullName);

colors.addEventListener("click", changeColor);
